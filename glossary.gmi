# Glossary

There are quite a few terms with specific meanings when talking about stred and subexes. This document is a reference to describe each term to help you get to grips with the tutorial/manual/source code.

### Array
A data structure consisting of an ordered list of values.

### Atom
An atom is the simplest component of data in stred. It is one of: null, a boolean, a number, a string terminal, a string rune or a terminal.

### Command
A single instruction telling stred what to do. stred is run with an ordered list of commands that for each token in the input, it will go through the commands and run them in order.

### Key
The string part of a map item. Keys in the input file are not treated as tokens.

### Map
A data structure consisting of an ordered list of pairs, each with a key and a value. Keys can be repeated.

### Number
A 64-bit float.

### Path
An ordered list of integers and strings that represents the route taken from the root node to a value or token.

### Path register
Every time a token is read from the input, this register is set to the path of the token translated into atoms.

### Register
A location that can store an ordered list of atoms. stred has 5 registers: path, token, X, Y and Z

### Root
The value that is at the outermost level of the input data. Every other value is nested somewhere inside the root.

### Scalar
A simple value. It is one of: null, a boolean, a number or a string. Scalars are somewhere between atoms and values.

### String
A scalar containing an ordered list of unicode codepoints. When split into atoms, it is made of a string terminal, followed by a string rune for each unicode codepoint, followed by another string terminal.

### String Rune
An atom representing a single unicode codepoint.

### String Terminal
An atom representing either the start or the end of a string.

### Subex
Short for substitute expression. A subex will be given a list of atoms as input, and depending on the contents of the subex, will either accept the input and produce a new list of atoms as output, or will just reject the input.

### Terminal
An atom marking either the beginning or the end of either a map or an array.

### Token
A single part of the input tree. It is either a scalar or a terminal. The input tree is processed one token at a time.

### Token register
Every time a token is read from the input, this register is set to that token translated into atoms.

### Value
A tree value. It is one of: null, a boolean, a number, a string, an array or a map.

### X, Y and Z registers
General purpose registers that have no specific purpose.
